import argparse
import json
import pprint
import requests
import sys
import urllib
import os
import re


with open('python/all_parks_yelp.json', encoding="utf8") as yelp_json:
	with open('json_data/all_parks.json', encoding="utf8") as json_data:
		with open('result.json', 'w') as f:
			d = json.load(json_data)
			givendata = d["data"]
			yelp = json.load(yelp_json)
			yelp_data = yelp["data"]

			for i in givendata:
				for j in yelp_data:
					if i["fullName"] in j['name']:
						for x in j:
							i[x] = j[x]

				f.write(json.dumps(i))
				f.write(",\n")