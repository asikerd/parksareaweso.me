# -*- coding: utf-8 -*-
"""
Yelp Fusion API code sample.

This program demonstrates the capability of the Yelp Fusion API
by using the Search API to query for businesses by a search term and location,
and the Business API to query additional information about the top result
from the search query.

Please refer to http://www.yelp.com/developers/v3/documentation for the API
documentation.

This program requires the Python requests library, which you can install via:
`pip install -r requirements.txt`.

Sample usage of the program:
`python sample.py --term="bars" --location="San Francisco, CA"`
"""
from __future__ import print_function

import argparse
import json
import pprint
import requests
import sys
import urllib
import os
import re


# This client code can run on Python 2.x or 3.x.  Your imports can be
# simpler if you only need one of those.
try:
    # For Python 3.0 and later
    from urllib.error import HTTPError
    from urllib.parse import quote
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2 and urllib
    from urllib2 import HTTPError
    from urllib import quote
    from urllib import urlencode


# Yelp Fusion no longer uses OAuth as of December 7, 2017.
# You no longer need to provide Client ID to fetch Data
# It now uses private keys to authenticate requests (API Key)
# You can find it on
# https://www.yelp.com/developers/v3/manage_app
API_KEY= 'p0iNRqFwa9mepetgxexKIXJhNrcfzoRAdmF4X1A_IxyeX_xySY2ZbUqudTSwPs68X4H90ekZ0mpNtm5NDg_5Whhnbfcja0E8qqRV5AVpAYdB024uBZhxWqTRCTIzW3Yx' 


# API constants, you shouldn't have to change these.
API_HOST = 'https://api.yelp.com'
SEARCH_PATH = '/v3/businesses/search'
BUSINESS_PATH = '/v3/businesses/'  # Business ID will come after slash.


# Defaults for our simple example.
DEFAULT_TERM = 'dinner'
DEFAULT_LOCATION = 'San Francisco, CA'
SEARCH_LIMIT = 3


def request(host, path, api_key, url_params=None):
    """Given your API_KEY, send a GET request to the API.

    Args:
        host (str): The domain host of the API.
        path (str): The path of the API after the domain.
        API_KEY (str): Your API Key.
        url_params (dict): An optional set of query parameters in the request.

    Returns:
        dict: The JSON response from the request.

    Raises:
        HTTPError: An error occurs from the HTTP request.
    """
    url_params = url_params or {}
    url = '{0}{1}'.format(host, quote(path.encode('utf8')))
    headers = {
        'Authorization': 'Bearer %s' % api_key,
    }

    #print(u'Querying {0} ...'.format(url))

    response = requests.request('GET', url, headers=headers, params=url_params)

    return response.json()

def get_business(api_key, business_id):
    """Query the Business API by a business ID.

    Args:
        business_id (str): The ID of the business to query.

    Returns:
        dict: The JSON response from the request.
    """
    business_path = BUSINESS_PATH + business_id

    return request(API_HOST, business_path, api_key)


def search(api_key, term, latitude, longitude):
    """Query the Search API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        location (str): The search location passed to the API.

    Returns:
        dict: The JSON response from the request.
    """

    url_params = {
        'term': term.replace(' ', '+'),
        'latitude': latitude.replace(' ', '+'),
        'longitude': longitude.replace(' ', '+'),
        'limit': SEARCH_LIMIT
    }
    return request(API_HOST, SEARCH_PATH, api_key, url_params=url_params)


def query_api(term, latitude, longitude):
    """Queries the API by the input values from the user.

    Args:
        term (str): The search term to query.
        location (str): The location of the business to query.
    """
    response = search(API_KEY, term, latitude, longitude)

    businesses = response.get('businesses')

    if not businesses:
        #print(u'No businesses for {0} in {1}, {2} found.'.format(term, latitude, longitude))
        return

    business_id = businesses[0]['id']

    #print(u'{0} businesses found, querying business info ' \
    #    'for the top result "{1}" ...'.format(
    #        len(businesses), business_id))
    response = get_business(API_KEY, business_id)

    #print(u'Result for business "{0}" found:'.format(business_id))
    return response


def main():

    with open('../json_data/all_parks.json', encoding="utf8") as json_data:
        print("{\"data\": [")
        d = json.load(json_data)
        givendata = d["data"]

        for i in givendata:
            lat_pos = True
            long_pos = True
            if not i["latLong"]:
                continue
            park_lat_long = i["latLong"]
            park_lat, park_long = park_lat_long.split(", ")
            if "-" in park_lat:
                lat_pos = False
            if "-" in park_long:
                long_pos = False
            park_lat = re.findall("\d+\.\d+", park_lat)
            park_long = re.findall("\d+\.\d+", park_long)     
            parklat = park_lat[0]
            parklong  = park_long[0]
            if not lat_pos:
                parklat = "-" + parklat
            if not long_pos:
                parklong = "-" + parklong
            try:
                park = query_api(i["fullName"], parklat, parklong)
                if park is None:
                    continue
                print(park, end=",")
            except HTTPError as error:
                sys.exit(
                    'Encountered HTTP error {0} on {1}:\n {2}\nAbort program.'.format(
                        error.code,
                        error.url,
                        error.read(),
                    )
                )
        print("]}")


if __name__ == '__main__':
    main()
