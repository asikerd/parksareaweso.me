# Read in the file
with open('python/all_parks_yelp.json', 'r') as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace("\\\"", "\"")
filedata = filedata.replace("\\\\\\\"", "\\\\\"")
#filedata = filedata.replace("\n", "")
#filedata = filedata.replace("    ", "")

# Write the file out again
with open('result.json', 'w') as file:
  file.write(filedata)