# coding: utf-8

import wikipedia

def removeNonAscii(s):
	return "".join(i for i in s if ord(i)<128)

with open("list", 'r') as data:
	with open("animals_list_na_2.json", 'w') as f:
		for row in data:
			x = wikipedia.page(row.replace("\n",""))
			print(x.title)
			if "List" in x.title:
				continue	
			if "International" in x.title:
				continue
			f.write("{\n")
			f.write("\"title\": \"" + x.title + "\",\n")
			f.write("\"summary\": \"" + removeNonAscii(x.summary.replace("\n", "").replace("\"", "\\\"")) + "\",\n")
			if len(x.images) > 0:
				f.write("\"image\": \"" + x.images[0] + "\"")
			f.write("},\n")


# LIST = wikipedia.page("List of mammals of North America")
# for animal in LIST.links:
# 	print(animal)
# with open("animals_list_na.json", 'w') as f:
# 	for animal in LIST.links:
# 		x = wikipedia.page(animal)
# 		print(x.title)
# 		if "List" in x.title:
# 			continue
# 		if "International" in x.title:
# 			continue
# 		f.write("{\n")
# 		f.write("\"title\": \"" + x.title + "\",\n")
# 		f.write("\"summary\": \"" + removeNonAscii(x.summary.replace("\n", "").replace("\"", "\\\"")) + "\",\n")
# 		f.write("\"image\": \"" + x.images[0] + "\"")
# 		f.write("},\n")