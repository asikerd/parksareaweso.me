Group Members:

	Austin Ikerd, asi327 asikerd;
	Victor Chau, vc7572 vcchau;
	Joseph Bess, jhb2649, josephbe;
	Kamran Khan, kak3595 kamran629; 
	Sean Yi, sjy324 sean_yi;




Git SHA: 6a6b020c9ebfee3afa3b6908dd5557bce817c54c

Please use Firefox when viewing our website.
link: http://parksareawesome.me/


est. completion time:

	Austin: 20 hrs
	Victor: 10 hrs
	Joseph: 20 hrs
	Kamran: 15
	Sean: 10 hrs

actual completion time:

	Austin: 15 hrs
	Victor: 15 hrs
	Joseph: 15 hrs
	Kamran: 15
	Sean: 15 hrs

comments:
	CREDITS:
    vine svg: https://codepen.io/denhai/pen/JXRLxG
    tree logo: http://clipart-library.com/public-domain-tree-cliparts.html
    match height: https://github.com/liabru/jquery-match-height

	Please use the browser firefox when using the website: When developing our website we did so with using firefox in mind, and we think it will probably work the best on that.  We asked one of the TAs and he said that it should be fine if we requested a sepcific browser to be used.

	Austin: This week was a lot less stressful than the past few weeks. We mostly just had to do D3 visualizations, which was not difficult. The main problem we had was the other group's API was offline so we had to wait until a few hours before the due time to complete the project.

	Victor: This week was a lot easier compared the previous phases. Implementation wise, we didn't have much left to add so most of the work was just creating our D3 diagrams.

	Joseph: This phase was a lot more fun then previous weeks.  We were able to work on the website how we wanted and I found doing some of the D3 stuff was pretty interesting.

	Kamran: This part of the phase was pretty tough, but we finally were able to get backend server working. This was a problem for us in the last phase as well but we finally figured it out. I learned a lot from this part of the project including running docker on AWS services and trying to get backend running on the ec2 server. I also learned about selienium testing and how that works.

	Sean: This phase was relatively easy, just had only a couple things to do left. Really enjoyed this project!
	API:
	    Get Park Data:
	        http://34.227.91.36:5000/api/parks
	    Get Wildlife Data:
            http://34.227.91.36:5000/api/wildlife
        Get Event Data:
            http://34.227.91.36:5000/api/events