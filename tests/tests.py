from index import app
from flask import url_for
import unittest
from flaskext.mysql import MySQL



class Flask_Tests(unittest.TestCase):

    def test_basic_test(self):
        # Test unit tests are properly functioning
        result = 2 + 2
        self.assertEqual(result, 4)
        
    def test_config_user(self):
        # Test whether sql database user is properly configured
        result = app.config['MYSQL_DATABASE_USER']
        self.assertEqual(result, 'vcchau')

    def test_config_pw(self):
        # Test whether sql database password is properly configured
        result = app.config['MYSQL_DATABASE_PASSWORD']
        self.assertEqual(result, 'parksareawesome')

    def test_config_database(self):
        # Test whether sql database is properly configured
        result = app.config['MYSQL_DATABASE_DB']
        self.assertEqual(result, 'parksareawesomedb')

    def test_config_host(self):
        # Test whether sql database host is properly configured
        result = app.config['MYSQL_DATABASE_HOST']
        self.assertEqual(result, 'parksareawesome.chr9q1gt6nxw.us-east-1.rds.amazonaws.com')

    def test_api_parks(self):
        """Test user successfully retrieved park api"""
        test = app.test_client(self)
        result = test.get('/api/parks')
        self.assertEqual(result.status_code, 200)

    def test_api_wildlife(self):
        """Test user successfully retrieved wildlife api"""
        test = app.test_client(self)
        result = test.get('/api/wildlife')
        self.assertEqual(result.status_code, 200)

    def test_api_events(self):
        """Test user successfully retrieved events api"""
        test = app.test_client(self)
        result = test.get('/api/events')
        self.assertEqual(result.status_code, 200)

if __name__ == "__main__":
    unittest.main()