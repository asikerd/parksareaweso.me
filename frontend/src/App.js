import React, { Component } from 'react';
import logo from './logo.svg';
import {BrowserRouter as Router } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import './App.css';
import Home from './components/Home.js';
import Wildlife from './components/wildlife.js';
import About from './components/aboutus.js';
import Main from './components/Main.js';
class App extends Component {
  render() {
    return (
      <Main/>
    );
  }
}

export default App;
