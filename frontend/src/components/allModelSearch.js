import React from 'react';
import { Link } from "react-router-dom";
import { Redirect, Switch } from 'react-router'
import Pagination from './pagination';
import {Thumbnail,Col,Button} from 'react-bootstrap';
import ParkSuggestions from "./ParkSuggestions";
import Park from './Park';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import './Parks.css';

const API_URL = "http://35.173.196.10:5000/api/parks/";
const searchTerm = "name";

export default class AllModelSearch extends React.Component{   

  constructor() {
    super();
    //button click for each item
    this.handleClick = this.handleClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.getInfo = this.getInfo.bind(this);
    this.toggle = this.toggle.bind(this);
    this.select = this.select.bind(this);

      // an example array of 30 items to be paged
    
    this.state = {
        parks: [],
        itemsToPaginate: [],
        pageOfItems: [],
        dropdownOpen: false,
        sortValue: 1,
        sortText: "Name A-Z"
    };
    
    // bind function in constructor instead of render 
    this.onChangePage = this.onChangePage.bind(this);
}
async componentDidMount() {
  await fetch(API_URL + 'parks')
    .then(results => {
        return results.json();
      }).then(data=> {
        this.setState({
          parks: data.objects,
        })
  });
  await this.getPaginationItems();
}

getPaginationItems() {
  const arr = [];
  var c = 0;
  for (var x = 0; x < 3; x++) {
    arr[c++] = (this.state.parks[x]);
    arr[c++] = (this.state.events[x]);
    arr[c++] = (this.state.wildlife[x]);
  }
  this.setState({
    itemsToPaginate: arr
  });
}

toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

async sortResults() {
  function byNameAsc(a, b) {
    return a.name > b.name;
  }
  function byNameDesc(a, b) {
    return a.name < b.name;
  }
  function byId(a, b) {
    return a.park_id - b.park_id;
  }
  function byRatingAsc(a, b) {
    return a.rating > b.rating;
  }
  function byRatingDesc(a, b) {
    return a.rating < b.rating;
  }

  console.log(this.state.sortValue);
  console.log(this.state.itemsToPaginate);
  var obj = [...this.state.itemsToPaginate];

  if (this.state.sortValue === 1) {
    await obj.sort(byNameAsc);
  }
  else if (this.state.sortValue === 2) {
    await obj.sort(byNameDesc);
  }
  else {
    await obj.sort(byNameAsc);
  }
  console.log(obj);
  this.state.itemsToPaginate = obj;
}


async getInfo() {
  let query = this.state.query;
  let info = this.state.allParks.filter((obj) => {
    for (var str of query.split(' ')) {
      if (obj.address.includes(str) ||
        obj.name.toLowerCase().includes(str) ||
        String(obj.park_id).toLowerCase().includes(str) ||
        String(obj.rating).toLowerCase().includes(str))
          return true;
    }
    return false;
  });
  await this.setState({
    parks: info
  });
  let info2 = this.state.allEvents.filter((obj) => {
    for (var str of query.split(' ')) {
      if (obj.address.includes(str) ||
        obj.name.toLowerCase().includes(str) ||
        String(obj.park_id).toLowerCase().includes(str) ||
        String(obj.rating).toLowerCase().includes(str))
          return true;
    }
    return false;
  });
  await this.setState({
    events: info2
  });
  let info3 = this.state.allWildlife.filter((obj) => {
    for (var str of query.split(' ')) {
      if (obj.address.includes(str) ||
        obj.name.toLowerCase().includes(str) ||
        String(obj.park_id).toLowerCase().includes(str) ||
        String(obj.rating).toLowerCase().includes(str))
          return true;
    }
    return false;
  });
  await this.setState({
    wildlife: info3
  });
  await this.sortResults();
  }


//handle button click 
handleClick() {
  this.setState(prevState => ({
    isToggleOn: !prevState.isToggleOn
  }));
}


  handleKeyPress(event) {
  if(event.key == 'Enter'){
    this.state.query = this.search.value;
    this.getInfo()
  }
  else {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
        // this.showDropdown()
        if (this.state.query.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.query) {
        // this.hideDropdown()
      }
    });
  }
}

async select(event) {
  console.log(event.target.innerText);
  console.log(event.target.value);
  await this.setState({
    dropdownOpen: !this.state.dropdownOpen,
    sortValue: parseInt(event.target.value),
    sortText: event.target.innerText
  });
  await this.sortResults();
  await this.handleClick();
  this.toggle();
  }


onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
}

render() {
  const park =this.state.pageOfItems.map(item =>
    <Col key ={item.park_id} xs={6} md={4}>
      <Thumbnail  src="https://images.unsplash.com/photo-1472061966262-e660a653324a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9935dc1f4701588bba11bea488a41826&auto=format&fit=crop&w=800&q=60" alt="242x200">
        <h5 id='park'>{item.park_name}</h5>
        <p>
          <Link to={"/parks/"+item.park_id}>
          <Button bsStyle="primary">Info</Button>&nbsp;
          </Link>
          
        </p>
      </Thumbnail>
    </Col>
    );
    return (
        <div>
          <form>
        <input
          placeholder="Search for..."
          ref={input => this.search = input}
          onKeyPress={this.handleKeyPress}
          className="search_input"
        />
        <ParkSuggestions results={this.state.parks} />
      </form>
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} onChange={this.handleChange} value={this.sortValue}>
        <DropdownToggle caret>
          Filter ({this.state.sortText})
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem value="1" onClick={this.select}>Name A-Z</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="2" onClick={this.select}>Name Z-A</DropdownItem>
        </DropdownMenu>
      </Dropdown>
            <div className="container">
                <div className="text-center">
                    <h1 id='events'>Search</h1>
                    <h3>Parks</h3>
                    {park}
                    <Pagination items={this.state.itemsToPaginate} onChangePage={this.onChangePage}/>
                </div>
            </div>
            <hr />
        </div>
    );
}

}



  