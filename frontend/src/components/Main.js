import { Route, NavLink,  HashRouter } from "react-router-dom";
import { Redirect, Switch } from 'react-router'
import React from 'react';
import Home from "./Home";
import Events from "./events";
import Wildlife from './wildlife';
import Parks from "./Parks";
import AboutUs from "./aboutus";
import logo from "../images/logo.png";
import PageNotFound from "./PageNotFound";
import Park from './Park';
import AllModelSearch from './allModelSearch';
import Animal from './Animal';
import event from './event';

export default class Main extends React.Component {
    render() {
      return (
        <HashRouter>
          
      <div>
      <div className="navbar navbar-inverse">
        <ul className="nav navbar-nav">
              <li><NavLink exact to="/" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'white'}}>Home</NavLink></li>
              <li><NavLink exact to="/parks" style={{color:'white'}}  activeClassName='wow'  activeStyle={{backgroundColor:'#5f6268'}}>Parks</NavLink></li>
              <li><NavLink exact to="/wildlife" style={{color:'white'}}  activeClassName='wow'  activeStyle={{backgroundColor:'#5f6268'}}>Wildlife</NavLink></li>
              <li><NavLink exact to="/events" style={{color:'white'}}  activeClassName='wow'  activeStyle={{backgroundColor:'#5f6268'}}>Events</NavLink></li>
              <li><NavLink exact to="/about" style={{color:'white'}}  activeClassName='wow'  activeStyle={{backgroundColor:'#5f6268'}}>About Us</NavLink></li>
              {/* <li><NavLink exact to="/search" style={{color:'white'}} activeClassName='wow' activeStyle={{backgroundColor:'#5f6268'}}>Search</NavLink></li> */}
            </ul>
            </div>
            <div className="content">
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/wildlife" component={Wildlife}/>
                <Route exact path="/events" component={Events}/>
                <Route exact path="/parks" component={Parks}/>
                <Route exact path="/about" component={AboutUs}/>
                <Route path="/parks/:park_id" component={Park}/>
                <Route path="/wildlife/:park_id" component={Animal}/>
                <Route path="/events/:park_id" component={event}/>
                <Route exact path="/search" component={AllModelSearch}/>
                <Route component={PageNotFound} />
              </Switch>
            </div>
          </div>
        </HashRouter>
      );
    }
  }