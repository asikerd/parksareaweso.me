import React from 'react';
import {Link} from 'react-router-dom';
import Pagination from './pagination';
import {Thumbnail,Col,Button} from 'react-bootstrap';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import EventsSuggestions from "./EventsSuggestions";

const API_URL = "http://35.173.196.10:5000/api/events"

export default class Events extends React.Component{   

  constructor() {
    super();
    //button click for each item
    this.handleClick = this.handleClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.getInfo = this.getInfo.bind(this);
    this.toggle = this.toggle.bind(this);
    this.select = this.select.bind(this);

      // an example array of 30 items to be paged
    
    this.state = {
        parks: [],
        allParks: [],
        pageOfItems: [],
        dropdownOpen: false,
        sortValue: 1,
        sortText: "Name A-Z"
    };
    
    // bind function in constructor instead of render 
    this.onChangePage = this.onChangePage.bind(this);
}
componentDidMount() {
  fetch(API_URL)
  .then(results => {
    return results.json();
  }).then(data=> this.setState({
    parks:data.objects,
    allParks:data.objects
  }));
}

toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }


async sortResults() {
  function byNameAsc(a, b) {
    return a.meetup_title.toLowerCase() > b.meetup_title.toLowerCase();
  }
  function byNameDesc(a, b) {
    return a.meetup_title.toLowerCase() < b.meetup_title.toLowerCase();
  }
  function byTimeAsc(a, b) {
    return a.meetup_time > b.meetup_time;
  }
  function byTimeDesc(a, b) {
    return a.meetup_time < b.meetup_time;
  }
  function byStateAsc(a, b) {
    return a.meetup_state ? b.meetup_state ? (a.meetup_state.toLowerCase() > b.meetup_state.toLowerCase()) : false : true;
  }
  function byStateDesc(a, b) {
    return a.meetup_state ? b.meetup_state ? (a.meetup_state.toLowerCase() < b.meetup_state.toLowerCase()) : true : false;
  }

  console.log(this.state.sortValue);
  var obj = [...this.state.parks];

  if (this.state.sortValue === 1) {
    await obj.sort(byNameAsc);
  }
  else if (this.state.sortValue === 2) {
    await obj.sort(byNameDesc);
  }
  else if (this.state.sortValue === 3) {
    await obj.sort(byTimeAsc);
  }
  else if (this.state.sortValue === 4) {
    await obj.sort(byTimeDesc);
  }
  else if (this.state.sortValue === 5) {
    await obj.sort(byStateAsc);
  }
  else if (this.state.sortValue === 6) {
    await obj.sort(byStateDesc);
  }
  else {
    await obj.sort(byNameAsc);
  }
  this.state.parks = obj;
}


async getInfo() {
  let query = this.state.query;
  let info = this.state.allParks.filter((obj) => {
    for (var str of query.split(' ')) {
      str = str.toLowerCase();
      // if (obj.park_address) 
      //   if (obj.park_address.includes(str))
      //     return true;
      if (obj.meetup_title)
        if(obj.meetup_title.toLowerCase().includes(str))
          return true;
      if (obj.meetup_group)
        if (obj.meetup_group.toLowerCase().includes(str))
          return true;
      if (obj.meetup_venue_name)
        if (obj.meetup_venue_name.toLowerCase().includes(str))
          return true;
    }
    return false;
  });
  await this.setState({
    parks: info
  });
  this.sortResults();
  }


//handle button click 
handleClick() {
  this.setState(prevState => ({
    isToggleOn: !prevState.isToggleOn
  }));
}


  handleKeyPress(event) {
  if(event.key == 'Enter'){
    this.state.query = this.search.value;
    this.getInfo()
  }
  else {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
        // this.showDropdown()
        if (this.state.query.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.query) {
        // this.hideDropdown()
      }
    });
  }
}

async select(event) {
  console.log(event.target.innerText);
  console.log(event.target.value);
  await this.setState({
    dropdownOpen: !this.state.dropdownOpen,
    sortValue: parseInt(event.target.value),
    sortText: event.target.innerText
  });
  await this.sortResults();
  await this.handleClick();
  this.toggle();
  }


onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
}

render() {
  var cardStyle = {
    display: 'block',
    transitionDuration: '0.3s',
    height: '25vw',
    overflow: 'hidden'
  }

  const park =this.state.pageOfItems.map(item =>
    <Link to={"/events/"+item.events_db_id}>
    <Col key ={item.events_db_id} xs={6} md={4}>
      <div className="card grid" style={cardStyle}>
        <img src={item.meetup_image} alt="242x200" className="card-img-top"></img>
        <p className='card-name important'>{item.meetup_title}</p>
        <p className='card-name'>{item.meetup_venue_name ? item.meetup_venue_name : ''}</p>
        <p className='card-name'>{(item.meetup_location ? item.meetup_location : '') + (item.meetup_state ? ', ' + item.meetup_state : '')}</p>
      </div>
    </Col>
    </Link>
    );
    return (
        <div>
          <form>
        <input
          placeholder="Search for..."
          ref={input => this.search = input}
          onKeyPress={this.handleKeyPress}
          className="search_input"
        />
        <EventsSuggestions results={this.state.parks} />
      </form>
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} onChange={this.handleChange} value={this.sortValue}>
        <DropdownToggle caret>
          Filter ({this.state.sortText})
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem value="1" onClick={this.select}>Name A-Z</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="2" onClick={this.select}>Name Z-A</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="3" onClick={this.select}>Time Asc</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="4" onClick={this.select}>Time Desc</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="5" onClick={this.select}>State A-Z</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="6" onClick={this.select}>State Z-A</DropdownItem>
        </DropdownMenu>
      </Dropdown>
            <div className="container">
                <div className="text-center">
                    <h1 id='events'>Events</h1>
                    {park}
                    <Pagination items={this.state.parks} onChangePage={this.onChangePage}/>
                </div>
            </div>
            <hr />
        </div>
    );
}

}



  