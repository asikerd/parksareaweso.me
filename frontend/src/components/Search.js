import React, { Component } from 'react'
import axios from 'axios'

import Suggestions from 'components/Suggestions'

const API_URL = "http://35.173.196.10:5000/api/parks";

class Search extends Component {
  state = {
    error: false,
    query: '',
    results: []
  }

  getInfo() {
    axios.get(`${API_URL}&${searchTerm}=${query}`)
      .then(({ data }) => {
        this.setState({
          results: data.data
        })
      })
      .catch(() => this.setState({ error: true }))
  }

  handleInputChange = () => {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
        // this.showDropdown()
        if (this.state.query.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.query) {
        // this.hideDropdown()
      }
    })
  }

  render() {
    return (
      <form>
        <input
          placeholder="Search for..."
          ref={input => this.search = input}
          onChange={this.handleInputChange}
        />
        <Suggestions results={this.state.results} />
      </form>
    )
  }
}

export default Search
