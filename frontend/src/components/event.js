import React from 'react';
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'

const API_URL = "http://35.173.196.10:5000/api/events/"

export default class Event extends React.Component{

  constructor() {
    super();
    this.convert_time = this.convert_time.bind(this)
    this.state = {
        Event: []
    };

}


convert_time(seconds) {
    if (!seconds) {
        return 0;
    }
    var d = new Date(0);
    d.setUTCSeconds(seconds/1000);
    return String(d);
}

componentDidMount() {
  fetch(API_URL+this.props.match.params.park_id)
  .then(results => {
    return results.json();
  }).then(data => {
    this.setState({
      Event: data
    })
    });
}


render(){
   
return (
  <div class="container">
    <h3 id="Name">{this.state.Event.meetup_title}</h3>
    <p id="summary">{this.state.Event.meetup_summary}</p>
    <p id="group">Group: {this.state.Event.meetup_group}</p>
    <p id="time">Time: {this.convert_time(this.state.Event.meetup_time)}</p>    
    <p id="address">Location: {this.state.Event.meetup_venue_name}, {this.state.Event.meetup_location}, {this.state.Event.meetup_state}, {this.state.Event.meetup_country}</p>
    <p id="link">Check out the event page here: <a href={this.state.Event.meetup_url} target = "_blank">{this.state.Event.meetup_url}</a></p>
    <img id="page-image" src={this.state.Event.meetup_image} alt="{this.state.Event.meetup_image}" className="page-image"></img>
  </div>       
);
}
}
