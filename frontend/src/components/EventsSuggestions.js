import React from 'react';
import { Link, Route, NavLink,  Router } from "react-router-dom";
import {Thumbnail,Col,Button} from 'react-bootstrap';

const EventsSuggestions = (props) => {
  const options = props.results.map(r => (
    <li key={r.events_db_id} className="suggestion">
    <Link to={"/parks/"+r.events_db_id}>
          <Button bsStyle="primary">{r.meetup_title}</Button>&nbsp;
     </Link>
    </li>
  ))
  return <ul>{options}</ul>
}

export default EventsSuggestions
