import React from 'react';
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'

const API_URL = "http://35.173.196.10:5000/api/parks/"

export default class parks extends React.Component{

  constructor() {
    super();
    
    this.state = {
        park: []
    };
}

componentDidMount() {
  fetch(API_URL+this.props.match.params.park_id)
  .then(results => {
    return results.json();
  }).then(data => {
    this.setState({
      park: data
    })
    });
}


render(){
   
return (
  <div class="container">
    <h3 id="Name">{this.state.park.park_name}</h3>
    <p id="summary">{this.state.park.park_description}</p>
    <p id="weather">{this.state.park.park_weather}</p>
    <p id="address">Address: {this.state.park.park_address}, {this.state.park.park_city}, {this.state.park.park_state}
    </p>
    <p id="rating">Rating: {this.state.park.park_rating} / 5.0 with {this.state.park.park_reviews} reviews 
    </p>
    <p>Cost: Free</p>
    <p id="contact">
        Contact us at: {this.state.park.park_phone_display}
    </p>
    <img id="page-image" src={this.state.park.park_image_url} alt={this.state.park.park_name} className="page-image"/>
  </div>       
);
}
}
