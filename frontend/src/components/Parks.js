import React from 'react';
import { Link } from "react-router-dom";
import { Redirect, Switch } from 'react-router'
import Pagination from './pagination';
import {Thumbnail,Col,Button} from 'react-bootstrap';
import ParkSuggestions from "./ParkSuggestions";
import Park from './Park';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import './Parks.css';

const API_URL = "http://35.173.196.10:5000/api/parks";
const searchTerm = "name";

export default class Parks extends React.Component{   

  constructor() {
    super();
    //button click for each item
    this.handleClick = this.handleClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.getInfo = this.getInfo.bind(this);
    this.toggle = this.toggle.bind(this);
    this.select = this.select.bind(this);

      // an example array of 30 items to be paged
    
    this.state = {
        parks: [],
        pageOfItems: [],
        dropdownOpen: false,
        sortValue: 1,
        sortText: "Name A-Z"
    };
    
    // bind function in constructor instead of render 
    this.onChangePage = this.onChangePage.bind(this);
}
componentDidMount() {
  fetch(API_URL)
  .then(results => {
    return results.json();
  }).then(data=> {
    this.setState({
      allParks: data.objects,
      parks: data.objects
  })
    });
}

toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

async sortResults() {
  function byNameAsc(a, b) {
    return a.park_name.toLowerCase() > b.park_name.toLowerCase();
  }
  function byNameDesc(a, b) {
    return a.park_name.toLowerCase() < b.park_name.toLowerCase();
  }
  function byRatingAsc(a, b) {
    return a.park_rating > b.park_rating;
  }
  function byRatingDesc(a, b) {
    return a.park_rating < b.park_rating;
  }
  function byStateAsc(a, b) {
    return a.park_state.toLowerCase() > b.park_state.toLowerCase();
  }
  function byStateDesc(a, b) {
    return a.park_state.toLowerCase() < b.park_state.toLowerCase();
  }

  console.log(this.state.sortValue);
  var obj = [...this.state.parks];

  if (this.state.sortValue === 1) {
    await obj.sort(byNameAsc);
  }
  else if (this.state.sortValue === 2) {
    await obj.sort(byNameDesc);
  }
  else if (this.state.sortValue === 3) {
    await obj.sort(byRatingAsc);
  }
  else if (this.state.sortValue === 4) {
    await obj.sort(byRatingDesc);
  }
  else if (this.state.sortValue === 5) {
    await obj.sort(byStateAsc);
  }
  else if (this.state.sortValue === 6) {
    await obj.sort(byStateDesc);
  }
  else {
    await obj.sort(byNameAsc);
  }
  this.state.parks = obj;
}


async getInfo() {
  let query = this.state.query;
  let info = this.state.allParks.filter((obj) => {
    for (var str of query.split(' ')) {
      str = str.toLowerCase();
      // if (obj.park_address) 
      //   if (obj.park_address.includes(str))
      //     return true;
      if (obj.park_name)
        if(obj.park_name.toLowerCase().includes(str))
          return true;
      if (obj.park_id)
        if (obj.park_id.toLowerCase().includes(str))
          return true;
    }
    return false;
  });
  await this.setState({
    parks: info
  });
  this.sortResults();
  }


//handle button click 
handleClick() {
  this.setState(prevState => ({
    isToggleOn: !prevState.isToggleOn
  }));
}


  handleKeyPress(event) {
  if(event.key == 'Enter'){
    this.state.query = this.search.value;
    this.getInfo()
  }
  else {
    this.setState({
      query: this.search.value
    }, () => {
      if (this.state.query && this.state.query.length > 1) {
        // this.showDropdown()
        if (this.state.query.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.query) {
        // this.hideDropdown()
      }
    });
  }
}

async select(event) {
  console.log(event.target.innerText);
  console.log(event.target.value);
  await this.setState({
    dropdownOpen: !this.state.dropdownOpen,
    sortValue: parseInt(event.target.value),
    sortText: event.target.innerText
  });
  await this.sortResults();
  await this.handleClick();
  this.toggle();
  }


onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
}

render() {
   var cardStyle = {
    display: 'block',
    transitionDuration: '0.3s',
    height: '25vw',
    overflow: 'hidden'
  }
  const park =this.state.pageOfItems.map(item =>
    <Link to={"/parks/"+item.park_db_id}>
    <Col key ={item.park_id} xs={6} md={4}>

      <div className="card grid" style={cardStyle}>

        <img src={item.park_image_url} alt={item.park_name} className="card-img-top"></img>
        <p className='card-name important'><b>{item.park_name}</b></p>
        <p className='card-name'>{item.park_city ? item.park_city + ", " : ""}{item.park_state}</p>
        <p className='card-name'>Rating: {item.park_rating ? item.park_rating + " / 5" : "N/A"}</p>
        <p>Cost: Free</p>
      </div>
    </Col>
    </Link>
    );
    return (
        <div>
          <form>
        <input
          placeholder="Search for..."
          ref={input => this.search = input}
          onKeyPress={this.handleKeyPress}
          className="search_input"
        />
        <ParkSuggestions results={this.state.parks} />
      </form>
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} onChange={this.handleChange} value={this.sortValue}>
        <DropdownToggle caret>
          Filter ({this.state.sortText})
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem value="1" onClick={this.select}>Name A-Z</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="2" onClick={this.select}>Name Z-A</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="3" onClick={this.select}>Rating Asc</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="4" onClick={this.select}>Rating Desc</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="5" onClick={this.select}>State A-Z</DropdownItem>
          <DropdownItem divider />
          <DropdownItem value="6" onClick={this.select}>State Z-A</DropdownItem>
        </DropdownMenu>
      </Dropdown>
            <div className="container">
                <div className="text-center">
                    <h1 id='events'>Parks</h1>
                    {park}
                    <Pagination items={this.state.parks} onChangePage={this.onChangePage}/>
                </div>
            </div>
            <hr />
        </div>
    );
}

}



  