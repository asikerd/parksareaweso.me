import React from 'react';
import { Link, Route, NavLink,  Router } from "react-router-dom";
import {Thumbnail,Col,Button} from 'react-bootstrap';

const ParkSuggestions = (props) => {
  const options = props.results.map(r => (
    <li key={r.park_db_id} className="suggestion">
    <Link to={"/parks/"+r.park_db_id}>
          <Button bsStyle="primary">{r.park_name}</Button>&nbsp;
     </Link>
    </li>
  ))
  return <ul>{options}</ul>
}

export default ParkSuggestions
