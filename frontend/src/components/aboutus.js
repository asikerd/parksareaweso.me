import React, { Component } from 'react';
import park2 from '../images/park2.jpg';
import $ from 'jquery';
import Austin from '../images/Austin.jpg';
import Joseph from '../images/joseph.jpg';
import Kamran from '../images/kamran.jpg';
import Sean from '../images/Sean.jpg';
import Victor from '../images/Victor.jpg';
import logo from '../images/logo.png';

class AboutUs extends Component {

  constructor(props) {
    super(props);
    //this.incrementCommits = this.incrementCommits.bind(this);
    //this.incrementIssues = this.incrementIssues.bind(this);
    this.iterateThroughGitlab = this.iterateThroughGitlab.bind(this);
  }



iterateThroughGitlab(parameters, results_per_page, page_number, check) {
    var temp_url = parameters.url;
    var headers = "";

    function incrementThings(name, check) {
      var div = "#" + name + "-" + check;
      $(div)[0].innerHTML = parseInt($(div)[0].innerHTML) + 1;
      $("#total_" + check + "s")[0].innerHTML = parseInt($("#total_" + check + "s")[0].innerHTML) + 1;
  }

    function getResponses(response, check) {
      for (var x in response) {
            var name = response[x].author_name ? response[x].author_name : response[x].author.name;
            var user = "";
          if (name === "Victor Chau") {
            user = "victor";
          }
          else if (name === "Austin Ikerd" || name === "voidiker66" || name === "AustinIkerd") {
            user = "austin";
          }
          else if (name === "joseph bess" || name === "Joseph Bess") {
            user = "joseph";
          }
          else if (name === "kamran629" || name === "Kamran A Khan") {
            user = "kamran";
          }
          else if (name === "sean_yi" || name === "Sean Yi") {
            user = "sean";
          }
          else {
            console.log(name + " not yet config");
            continue;
          }
          incrementThings(user, check)
        }
    }


    parameters.url += "?per_page=" + results_per_page + "&page=" + page_number;
    $.ajax(parameters).done(function(response, textStatus, xhr) {
        console.log(check);
        getResponses(response, check);
        headers = xhr.getAllResponseHeaders();
        console.log("one: " + headers);
        var arr = headers.trim().split(/[\r\n]+/);

        // Create a map of header names to values
        var headerMap = {};
        arr.forEach(function (line) {
          var parts = line.split(': ');
          var header = parts.shift();
          var value = parts.join(': ');
          headerMap[header] = value;
        });
        //console.log(headerMap);
        for (var y = 2; y < headerMap["X-Total"]; y++) {
          parameters.url = temp_url + "?per_page=" + results_per_page + "&page=" + y;
          $.ajax(parameters).done(function(response, textStatus, xhr) {
            getResponses(response, check);
        });
        }
      });
}

componentDidMount() {
  var parameters = {
    url: "https://gitlab.com/api/v4/projects/7229879/repository/commits",
    async: true,
    crossDomain: true,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    method: "GET",
  };
  this.iterateThroughGitlab(parameters, 100, 1, "commit");

  var parameters2 = {
    url: "https://gitlab.com/api/v4/projects/7229879/issues",
    async: true,
    crossDomain: true,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    method: "GET",
  };
  this.iterateThroughGitlab(parameters2, 1000, 1, "issue");
}

 render() {
    return (
<div className="container">
  <h1>About Us</h1>
  <div className="row">
    <div className="col-lg-6">
      <h2>Welcome to Parks Are Awesome</h2>
      <p fontSize='18px'>
        Our mission is to promote the amazing parks located throughout the United States and raise awareness about the problems that
        they may face. There is so much beauty to be found in the wildlife and plantlife that live in parks. Our hope is
        that people of all ages will use our service to connect with nature and enjoy parks. Parks Are Awesome provides users
        with an interface to find local parks and search for nearby community events and volunteering opportunities taking place.
        We also provide users with a database of the flora and fauna native to regions throughout the U.S.
      </p>
      <p fontSize='18px'>
        What are you waiting for? Find an awesome park near you today!
      </p>
    </div>
    <div className="col-lg-6">
      <br/>
      <img src={park2} alt="Park" className="park_about_us"></img>
    </div>
  </div>
  <hr/>
  <h2> Meet the developers!</h2>
  <br/>
  <div className="row">

<div className='desktop circle-container'>
  <div className='center'><img src={logo}></img></div>
  <div className='deg0 profile_hover'>
    <img src={Austin} alt="Austin"></img>
    <div>
      <h3>
        <b>Austin Ikerd</b>
      </h3>
    </div>
    <div>
      <p>
        Austin is a 4th year Computer Science student who loves video games, terrible biographies, and his cute cat.
      </p>
          <hr/>
          <h4>Responsibilities</h4>
          <h4>No. of commits:&nbsp;
            <span id="austin-commit">0</span>
          </h4>
          <h4>No. of issues:&nbsp;
            <span id="austin-issue">0</span>
          </h4>
          <h4>No. of unit tests:&nbsp;</h4>
    </div>
  </div>
  <div className='deg72 profile_hover'>
    <img src={Sean} alt="Sean"></img>
    <div>
      <h3>
        <b>Sean Yi</b>
      </h3>
    </div>
    <div>
      <p>Sean is a fourth-year CS Major at UT Austin. He enjoys eating. A lot.</p>
      <hr/>
      <h4>Responsibilities</h4>
      <h4>No. of commits:&nbsp;
        <span id="sean-commit">0</span>
      </h4>
      <h4>No. of issues:&nbsp;
        <span id="sean-issue">0</span>
      </h4>
      <h4>No. of unit tests:&nbsp;</h4>
    </div>
  </div>
  <div className='deg144 profile_hover'>
    <img src={Victor}></img>
    <div>
      <h3>
        <b>Victor Chau</b>
      </h3>
    </div>
    <div>
          <p>Victor is a 4th year CS student at UT Austin and loves eating and sports.</p>
          <hr/>
          <h4>Responsibilities</h4>
          <h4>No. of commits:&nbsp;
            <span id="victor-commit">0</span>
          </h4>
          <h4>No. of issues:&nbsp;
            <span id="victor-issue">0</span>
          </h4>
          <h4>No. of unit tests:&nbsp; 6</h4>
    </div>
  </div>
  <div className='deg216 profile_hover'>
    <img src={Kamran}></img>
    <div>
    <h3>
          <b>Kamran Khan</b>
        </h3>
      </div>
      <div>
        <p>I'm currently a senior studying computer science at UT Austin. I enjoy playing poker, eating different types of food,
          and playing basketball. \m/</p>
        <hr/>
        <h4>Responsibilities</h4>
        <h4>No. of commits:&nbsp;
          <span id="kamran-commit">0</span>
        </h4>
        <h4>No. of issues:&nbsp;
          <span id="kamran-issue">0</span>
        </h4>
        <h4>No. of unit tests:&nbsp;</h4>
      </div>
  </div>
  <div className='deg288 profile_hover'>
    <img src={Joseph}></img>
    <div>
    <h3>
            <b>Joseph Bess</b>
          </h3>
        </div>
        <div>
          <p>Joey is a 4th year computer science and mathmatics major at UT.  In his free time he enjoys sleeping and video games.</p>
          <hr/>
          <h4>Responsibilities</h4>
          <h4>No. of commits:&nbsp;
            <span id="joseph-commit">0</span>
          </h4>
          <h4>No. of issues:&nbsp;
            <span id="joseph-issue">0</span>
          </h4>
          <h4>No. of unit tests:&nbsp; 30</h4>
  </div>
</div>

</div>




  <h2>Website Statistics</h2>
  <div className="row">
    <div className="container">
      <h4><b>Total no. of commits: <span id="total_commits">0</span></b></h4>
      <h4><b>Total no. of issues: <span id="total_issues">0</span></b></h4>
      <h4><b>Total no. of unit tests: <span id="total_unit_tests">36</span></b></h4>
      <h4><b>GitLab Repo: </b><a href="https://gitlab.com/asikerd/parksareaweso.me" target="_blank">
        https://gitlab.com/asikerd/parksareaweso.me</a></h4>
        <h4><b>Restful API: </b><a href="https://documenter.getpostman.com/view/4706698/RWMCt9fN" target="_blank">
       https://documenter.getpostman.com/view/4703107/RWEmHvvT</a></h4>
     <h3><u><b>Languages used:</b></u></h3>
     <h4><p>
        <br/>
        <b>Python</b> - Used for creating and inserting data into our database.</p>
        <p><b>JavaScript</b> - Used for our front-end components.</p>
        <p><b>HTML/CSS</b> - Used to develop and style the front end of the website.
        <br/></p></h4>

        <h3><u><b>Tools used:</b></u></h3>
        <h4><p>
        <br/>
        <b>GitLab</b> - GitLab is a git repository manager that we used for version control, continuous integration testing through their CI/CD, and keeping track of bugs and issues.</p>
        <p><b>Postman</b> - Postman is an API tool that we used get data from our data sources and document our API.</p>
        <p><b>Amazon Web Services</b> - AWS is a cloud-hosting platform that we used for hosting our website.</p>
        <p><b>MySQL</b> - MySQL is a relational database management system that we used to create and manage our database.</p>
        <p><b>SQLAlchemy</b> - SQLAlchemy is an SQL toolkit for Python that we used together with Flask to retrieve data from our database.</p>
        <p><b>Docker</b> - Docker is a containerization service that we used to run our front-end and back-end servers together with Amazon Web Services.</p>
        <p><b>React</b> - React is a JavaScript framework that we used to render front-end web components such as our model's information card.</p>
        <p><b>Flask</b> - Flask is micro web framework written in Python that we used to host our back-end database and RESTful API.</p>
        <p><b>PlantUML</b> - PlantUML is a tool that we used to create our UML diagrams.</p>
        <p><b>Piazza</b> - Piazza is an open discussion board and Q&amp;A web service that we used to retrieve user stories and feedback from our customers.</p>
        </h4>
      <h3><u><b>Testing tools:</b></u><br /></h3>
      <h4><p>
        <br/>
        <b>GitLab</b> - GitLab's Continuous Integration allows you to upload your own test files to run through automated testing after each commit.</p>
        <p><b>Mocha</b> - Mocha is a JavaScript testing framework that we used to test our JavaScript/React code</p>
        <p><b>Nosetests</b> - Nosetests is a Python testing framework that we used to test our Flask code.</p>
        <p><b>Selenium</b> - Selenium is an automated web testing service that we used to test our website's GUI.</p>
        <p><b>Postman</b> - Postman also provides a framework for writing and running JavaScript tests which we used to test our RESTful API.</p>
      </h4>
     <h3><u><b>Data sources:</b></u></h3>
     <h4>
        <p><a href="https://www.meetup.com/" target="_blank">Meetup.com</a> - Found events related to outdoors/saving the environment and the api gave us all the information for that particular event, which we used for our Volunteer ops section.</p>
        <p><a href="https://www.nps.gov/index.htm" target="_blank">NPS.gov</a> - Information gathered by finding the park code for a particular park we wanted information on and using an api call to get the name and description of the park.</p>
        <p><a href="https://www.wikipedia.org/" target="_blank">Wikipedia.org</a> - Found a particular article about wildlife and used the wikipedia api to obtain a description and image of a particular plant or animal.</p>
        <p><a href="https://www.yelp.com/developers/documentation/v3" target="_blank">Yelp.com</a> - Found information such as the park's phone number and rating on Yelp.</p>
      </h4>
      <h4><b>Our Technical Report can be found <a href="https://gitlab.com/asikerd/parksareaweso.me/blob/master/Techincal%20Report.pdf" target="_blank">here</a>.</b></h4>
      <h4><b>Our D3 Diagrams can be found here: </b>
        <p><a href="http://d3-diagram.s3-website-us-east-1.amazonaws.com/usa_map_d3vis.html" target="_blank">Parks</a></p>
        <p><a href="http://d3-diagram.s3-website-us-east-1.amazonaws.com/usa_map_events.html" target="_blank">Events</a></p>
        <p><a href="http://d3-diagram.s3-website-us-east-1.amazonaws.com/" target="_blank">Wildlife</a></p>
      </h4>
      <h4><b>Our Developer's D3 Diagrams can be found here: </b>
        {/* <p><a href="https://s3.amazonaws.com/d3-diagram/healthconditions.html" target="_blank">Health Conditions</a></p> */}
        <p><a href="http://d3-diagram.s3-website-us-east-1.amazonaws.com/charaties_loc.html" target="_blank">Charities</a></p>
        <p><a href="http://d3-diagram.s3-website-us-east-1.amazonaws.com/medications.html" target="_blank">Medications</a></p>
      </h4>
    </div>
  </div>
</div>
</div>

);
}
}

export default AboutUs;