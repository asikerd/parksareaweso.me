import React from 'react';
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'

const API_URL = "http://35.173.196.10:5000/api/wildlife/"

export default class Animal extends React.Component{

  constructor() {
    super();
    
    this.state = {
        park: []
    };
}

componentDidMount() {
  fetch(API_URL+this.props.match.params.park_id)
  .then(results => {
    return results.json();
  }).then(data => {
    this.setState({
      park: data
    })
    });
}


render(){
   
return (
  <div class="container">
    <h3 id="Name">{this.state.park.wiki_title}</h3>
    <p id="summary">{this.state.park.wiki_summary}</p>
    <img id="page-image" src={this.state.park.wiki_image} alt="{this.state.park.wiki_title}" className="page-image"></img>
  </div>       
);
}
}
