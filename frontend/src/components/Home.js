import React, { Component } from 'react';
import park1 from '../images/park4.jpg';
import park2 from '../images/park2.jpg';
import park3 from '../images/park5.jpg';

export default class Home extends React.Component {
  render() {
    return (
    	<div>
      	<div id="gradient" className="desktop"></div> 

    	<span className="container emphasis">
  <h1>Parks Are Awesome</h1>
  <p className="text-center">And that is why we are trying to save them!</p>
</span>

<div className="container">
<div className="row">
  <div className="col-md-12">
    <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
      <ol className="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <div className="carousel-inner" role="listbox">
        <div className="item active">
          <img src={park1} alt=""></img>
        </div>
        <div className="item">
          <img src={park2} alt=""></img>
        </div>
        <div className="item">
          <img src={park3} alt=""></img>
        </div>
      </div>

      <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>

    </div>
  </div>
</div>
</div>
</div>
    	    );
  }
}